﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Compression;
using System.IO;
using Ionic.Zip;
namespace BackupPeriods
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length >= 2)
            {
                string backupDir = args[0];
                int aantalDagen = 0;
                Int32.TryParse(args[1], out aantalDagen);

                using (ZipFile zip = new ZipFile())
                {

                    zip.AddDirectory(Algemeen.Parameter.SSbRoot + "vst", "vst");
                    zip.AddDirectory(Algemeen.Parameter.SSbRoot + "var", "var");
                    zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                    if (!Directory.Exists(backupDir))
                    {
                        Directory.CreateDirectory(backupDir);
                    }
                    zip.Save(backupDir + "\\Backup_" + DateTime.Now.ToString("yyyyMMdd") + ".zip");
                }

                string sql = string.Empty;
                for (int i = 0; i < 13; i++)
                {
                    sql += "totdat[" + i.ToString() + "] ";
                }
                Dictionary<string, List<string>> dict = Algemeen.Proces.DlsSqlSel("yyyy " + sql + "from pdedef where yyyy = " + DateTime.Now.Year.ToString());
                foreach (string s in dict[DateTime.Now.Year.ToString()])
                {
                    if (Algemeen.Proces.dtStringToDate(s).AddDays(-2).DayOfYear == DateTime.Now.DayOfYear)
                    {
                        if (!Directory.Exists(backupDir + "\\" + DateTime.Now.Year.ToString()))
                        {
                            Directory.CreateDirectory(backupDir + "\\" + DateTime.Now.Year.ToString());
                        }
                        File.Copy(backupDir + "\\Backup_" + DateTime.Now.ToString("yyyyMMdd") + ".zip", backupDir + "\\" + DateTime.Now.Year.ToString() + @"\Backup_" + DateTime.Now.ToString("yyyyMMdd") + ".zip", true);

                    }

                }
                foreach (string file in Directory.GetFiles(backupDir, "Backup_*.zip"))
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.CreationTime < DateTime.Now.AddDays(-aantalDagen))
                    {
                        fi.Delete();
                    }
                }
            }
            else
            {
                Console.WriteLine("Not enough arguments (Backupdirectory Days)");
            }
        }
        
    }
}
